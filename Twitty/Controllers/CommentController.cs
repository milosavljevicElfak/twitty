﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Neo4j.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Twitty.Extensions;
using Twitty.Models;
using Twitty.Session;

namespace Twitty.Controllers
{
    public class CommentController : Controller
    {
        private readonly IDriver _driver;

        public CommentController(IDriver driver)
        {
            _driver = driver;
        }

        [HttpPost]
        public async Task<IActionResult> AddComment(string commentInfo, int postId)
        {
            var userId = HttpContext.Session.GetInt32(TwittySessionKeys.UserId) ?? -1;
            if(HttpContext.Session.IsUsernameEmpty() || userId == -1)
                return RedirectToAction("Login");

            var statementText = new StringBuilder();
            statementText.Append(@$"
                MATCH (user:User), (post:Post) WHERE id(user)= {userId} AND id(post)= {postId}
                CREATE (user)-[x:COMMENTED {{commentInfo: '{commentInfo.Replace("'", "")}', whenCommented: '{DateTime.Now}' }}]->(post)");

            var session = _driver.AsyncSession();
            var result = await session.WriteTransactionAsync(tx => tx.RunAsync(statementText.ToString()));
            return RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> EditComment(string commentInfo, int commentId)
        {
            if(HttpContext.Session.IsUsernameEmpty())
                return RedirectToAction("Login");

            var statementText = new StringBuilder();
            statementText.Append($"MATCH ()-[c]->() where id(c) = ${commentId} set c.commentInfo = '${commentInfo}'");

            var session = _driver.AsyncSession();
            var result = await session.WriteTransactionAsync(tx => tx.RunAsync(statementText.ToString()));
            return RedirectToAction("Inedx", "Home");
        }

        public async Task<IActionResult> DeleteComment(int commentId)
        {
            int userId = HttpContext.Session.GetInt32(TwittySessionKeys.UserId) ?? -1;
            if(HttpContext.Session.IsUsernameEmpty() || userId == -1)
                return RedirectToAction("Login");

            var statementText = new StringBuilder();
            statementText.Append($"MATCH (u)-[com]->() WHERE id(com)={commentId} and id(u)={userId} DELETE com");
           
            var session = this._driver.AsyncSession();
            var result = await session.WriteTransactionAsync(tx => tx.RunAsync(statementText.ToString()));
            return RedirectToAction("Index", "Home");
        }

    }
}
