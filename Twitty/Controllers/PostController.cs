﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Neo4j.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Twitty.Extensions;
using Twitty.Models;
using Twitty.Session;
using Post = Twitty.Models.Post;

namespace Twitty.Controllers
{
    namespace Twitty.Controllers
    {
        public class PostController : Controller
        {
            private readonly IDriver _driver;

            public PostController(IDriver driver)
            {
                _driver = driver;
            }      

            [HttpPost]
            public async Task<IActionResult> AddPost(string text)
            {
                string username = HttpContext.Session.GetString(TwittySessionKeys.Username);
                int userId = HttpContext.Session.GetInt32(TwittySessionKeys.UserId) ?? -1;
                if (string.IsNullOrEmpty(username) || userId == -1)
                    return RedirectToAction("Login");   


                var statementText = new StringBuilder();
                statementText.Append(@$"MATCH (u: User) where id(u) = {userId}
                                        CREATE (u)-[:POSTED]->(post:Post {{text: '{text.Replace("'", "")}', creationDate: '{DateTime.Now}'}})");

                var session = _driver.AsyncSession();
                var result = await session.WriteTransactionAsync(tx => tx.RunAsync(statementText.ToString()));

                return RedirectToAction("Index", "Home");
            }
        }
    }
}
