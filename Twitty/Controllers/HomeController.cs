﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Neo4j.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Twitty.Extensions;
using Twitty.Models;
using Twitty.Session;

namespace Twitty.Controllers
{
    public class HomeController : Controller
    {
        private readonly IDriver _driver;

        public HomeController(IDriver driver)
        {
            _driver = driver;
        }

        public async Task<IActionResult> Index()
        {            
            if(HttpContext.Session.IsUsernameEmpty())
                return RedirectToAction("Login");

            ViewData["userId"] = HttpContext.Session.GetInt32(TwittySessionKeys.UserId);

            string cypherQuery = @"MATCH (u:User)-[:POSTED]->(p:Post)<-[c:COMMENTED]-(uwc:User) return u, p, c, uwc
                                    UNION
                                    MATCH (u:User)-[:POSTED]->(p:Post) return u, p, null as c, null as uwc";

            IList<Post> postsModal = new List<Post>();
            IResultCursor result;
            IAsyncSession session = _driver.AsyncSession();
            try
            {
                result = await session.RunAsync(cypherQuery);
                var records = await result.ToListAsync();
                records.ForEach(rec =>
                {
                    
                    INode user = rec["u"].As<INode>();
                    INode post = rec["p"].As<INode>();
                    INode userWhoCommented = rec["uwc"].As<INode>();
                    IRelationship comment = rec["c"].As<IRelationship>();

                    Post postAlreadyExists = postsModal.FirstOrDefault(postModel => postModel.Id == post.Id);
                    var index = postsModal.IndexOf(postAlreadyExists);

                    

                    Comment commentForPost = null;
                    if(comment != null)
                    {
                        User userWhoCommentedModal = new User()
                        {
                            Id = (int)userWhoCommented.Id,
                            Username = userWhoCommented.Properties["username"].As<string>()
                        };
                        commentForPost = new Comment()
                        {
                            Id = (int)comment.Id,
                            CommentInfo = comment["commentInfo"].As<string>(),
                            WhenCommented = comment["whenCommented"].As<DateTime>(),
                            UserWhoCommented = userWhoCommentedModal
                        };
                    }

                    if(postAlreadyExists != null)
                    {
                        if(commentForPost != null)
                        {
                            postsModal[index].Comments.Add(commentForPost);
                        }
                    }
                    else
                    {
                        User userModel = new User()
                        {
                            Id = (int)user.Id,
                            Username = user.Properties["username"].As<string>(),
                        };
                        Post postModel = new Post()
                        {
                            Id = (int)post.Id,
                            Text = post.Properties["text"].As<string>(),
                            CreationDate = post.Properties["creationDate"].As<DateTime>(),
                            MyUser = userModel,
                        };

                        if(commentForPost != null)
                        {
                            postModel.Comments.Add(commentForPost);
                        }

                        postsModal.Add(postModel);
                    }
                });
            }
            finally
            {
                await session.CloseAsync();
            }


            return View(postsModal.OrderByDescending(post => post.CreationDate).ToList());
        }

        public async Task<IActionResult> Followers()
        {
            int userId = HttpContext.Session.GetInt32(TwittySessionKeys.UserId) ?? -1;
            if(HttpContext.Session.IsUsernameEmpty() || userId == -1)
                    return RedirectToAction("Login");

            ViewData["userId"] = HttpContext.Session.GetInt32(TwittySessionKeys.UserId);

            string cypherQuery = $@"MATCH (mu:User)-[:FOLLOW]->(u:User)-[:POSTED]->(p:Post)<-[c:COMMENTED]-(uwc:User)
                                        where id(mu)={userId} return u, p, c, uwc
                                    UNION
                                    MATCH (mu:User)-[:FOLLOW]->(u:User)-[:POSTED]->(p:Post) 
                                        where id(mu)={userId} return u, p, null as c, null as uwc";

            IList<Post> postsModal = new List<Post>();
            IResultCursor result;
            IAsyncSession session = _driver.AsyncSession();
            try
            {
                result = await session.RunAsync(cypherQuery);
                var records = await result.ToListAsync();
                records.ForEach(rec =>
                {
                    
                    INode user = rec["u"].As<INode>();
                    INode post = rec["p"].As<INode>();
                    INode userWhoCommented = rec["uwc"].As<INode>();
                    IRelationship comment = rec["c"].As<IRelationship>();

                    Post postAlreadyExists = postsModal.FirstOrDefault(postModel => postModel.Id == post.Id);
                    var index = postsModal.IndexOf(postAlreadyExists);

                    

                    Comment commentForPost = null;
                    if(comment != null)
                    {
                        User userWhoCommentedModal = new User()
                        {
                            Id = (int)userWhoCommented.Id,
                            Username = userWhoCommented.Properties["username"].As<string>()
                        };
                        commentForPost = new Comment()
                        {
                            Id = (int)comment.Id,
                            CommentInfo = comment["commentInfo"].As<string>(),
                            WhenCommented = comment["whenCommented"].As<DateTime>(),
                            UserWhoCommented = userWhoCommentedModal
                        };
                    }

                    if(postAlreadyExists != null)
                    {
                        if(commentForPost != null)
                        {
                            postsModal[index].Comments.Add(commentForPost);
                        }
                    }
                    else
                    {
                        User userModel = new User()
                        {
                            Id = (int)user.Id,
                            Username = user.Properties["username"].As<string>(),
                        };
                        Post postModel = new Post()
                        {
                            Id = (int)post.Id,
                            Text = post.Properties["text"].As<string>(),
                            CreationDate = post.Properties["creationDate"].As<DateTime>(),
                            MyUser = userModel,
                        };

                        if(commentForPost != null)
                        {
                            postModel.Comments.Add(commentForPost);
                        }

                        postsModal.Add(postModel);
                    }
                });
            }
            finally
            {
                await session.CloseAsync();
            }


            return View(postsModal.OrderByDescending(post => post.CreationDate).ToList());
        }

        public async Task<IActionResult> UserAsync(int userId)
        {
            var currentUserId = HttpContext.Session.GetInt32(Session.TwittySessionKeys.UserId);
            if(currentUserId == -1 || HttpContext.Session.IsUsernameEmpty())
                return RedirectToAction("Login");

            ViewData["userId"] = HttpContext.Session.GetInt32(TwittySessionKeys.UserId);

            string cypherQuery = $@"MATCH (u)-[:POSTED]->(p:Post)<-[c:COMMENTED]-(uwc:User) where id(u)={userId} return u, p, c, uwc
                                    UNION
                                    MATCH (u)-[:POSTED]->(p:Post) where id(u)={userId} return u, p, null as c, null as uwc";


            IList<Post> postsModal = new List<Post>();
            ViewData["displayFollow"] = null;
            IResultCursor result;
            IAsyncSession session = _driver.AsyncSession();
            try
            {
                result = await session.RunAsync(cypherQuery);
                var records = await result.ToListAsync();
                records.ForEach(rec =>
                {
                    
                    INode user = rec["u"].As<INode>();
                    INode post = rec["p"].As<INode>();
                    INode userWhoCommented = rec["uwc"].As<INode>();
                    IRelationship comment = rec["c"].As<IRelationship>();

                    Post postAlreadyExists = postsModal.FirstOrDefault(postModel => postModel.Id == post.Id);
                    var index = postsModal.IndexOf(postAlreadyExists);

                    

                    Comment commentForPost = null;
                    if(comment != null)
                    {
                        User userWhoCommentedModal = new User()
                        {
                            Id = (int)userWhoCommented.Id,
                            Username = userWhoCommented.Properties["username"].As<string>()
                        };
                        commentForPost = new Comment()
                        {
                            Id = (int)comment.Id,
                            CommentInfo = comment["commentInfo"].As<string>(),
                            WhenCommented = comment["whenCommented"].As<DateTime>(),
                            UserWhoCommented = userWhoCommentedModal
                        };
                    }

                    if(postAlreadyExists != null)
                    {
                        if(commentForPost != null)
                        {
                            postsModal[index].Comments.Add(commentForPost);
                        }
                    }
                    else
                    {
                        User userModel = new User()
                        {
                            Id = (int)user.Id,
                            Username = user.Properties["username"].As<string>(),
                        };
                        Post postModel = new Post()
                        {
                            Id = (int)post.Id,
                            Text = post.Properties["text"].As<string>(),
                            CreationDate = post.Properties["creationDate"].As<DateTime>(),
                            MyUser = userModel,
                        };

                        if(commentForPost != null)
                        {
                            postModel.Comments.Add(commentForPost);
                        }

                        postsModal.Add(postModel);
                    }
                });
            }
            finally
            {
                await session.CloseAsync();
            }

            if(postsModal.Count == 0)
                return RedirectToAction("Index");

            if(userId != currentUserId)
            {
                string cypherQuery2 = $@"MATCH (u)-[:FOLLOW]->(uu) where id(u)={currentUserId} and id(uu)={userId} return count(*) as c";
                session = _driver.AsyncSession();
                try
                {
                    result = await session.RunAsync(cypherQuery2);
                    var record = await result.ToListAsync();
                    ViewData["displayFollow"] = record[0]["c"].As<int>() == 0;
                }
                finally
                {
                    await session.CloseAsync();
                }
            }

            
            return View(postsModal.OrderByDescending(post => post.CreationDate).ToList());
        }

        public IActionResult Login()
        {
            if(!HttpContext.Session.IsUsernameEmpty())
                return RedirectToAction("Index");

            return View();
        }

        public IActionResult Register()
        {
            if(!HttpContext.Session.IsUsernameEmpty())
                return RedirectToAction("Index");

            return View();
        }
    }
}
