﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Twitty.Session
{
    public class TwittySessionKeys
    { 
        public static string Username => "_sessionUserName";

        public static string UserId => "_sessionUserId";
    }
}
