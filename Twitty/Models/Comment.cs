﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Twitty.Models
{
    public class Comment
    {
          public int Id { get; set; }

          public string CommentInfo { get; set; }

          public DateTime WhenCommented { get; set; }

          public User UserWhoCommented { get; set; }

    }
}
