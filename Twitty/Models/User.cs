﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Twitty.Models
{
    public class User
    {
        public User()
        {
        }

        public int Id { get; set; }
        public string Username { get; set; }

    }
}
