﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Twitty.Models
{
    public class Post
    {
        public Post()
        {
            Comments = new List<Comment>();
        }

        public int Id { get; set; }

        public string Text { get; set; }

        public IList<Comment> Comments { get; set; }

        public DateTime CreationDate{get;set;}

        public User MyUser { get; set; }
    }
}
