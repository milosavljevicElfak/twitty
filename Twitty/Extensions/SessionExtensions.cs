﻿using Microsoft.AspNetCore.Http;
using Twitty.Session;

namespace Twitty.Extensions
{
    public static class SessionExtensions
    {
        public static bool IsUsernameEmpty(this ISession session)
        {
            return string.IsNullOrEmpty(session.GetString(TwittySessionKeys.Username));
        }
    }
}
